# OpenML dataset: Nebraska-Football-Box-Scores-1962-2020

https://www.openml.org/d/43580

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Dataset includes date and time, opponent, score, rushing statistics, passing statistics, turnovers, Nebraska penalties, point spread, and weather information. All games are included. Penalty data is incomplete before 1972-09-16 (Texas AM). Point spread data is unknown before the 1978 season. It is listed for approx. 89 of games from from that point forward. Weather data is taken from the DarkSky API and from Weather Underground. I've found temperature and humidity data to be fairly reliable but wind data is less so.
Column Labels:

date  Date the game was played
time  Kickoff (CT)
opp  Nebraska's opponent
site  Location the game was played (home, away, neutral-home, or neutral-away)
conference  Whether it was a conference opponent (TRUE or FALSE)
opp-score  Nebraska's opponent's score
ne-score  Nebraska's score
opp-rush-att  Opponent rushing attempts
opp-rush-yards  Opponent rushing yards
ne-rush-att  Nebraska rushing attempts
ne-rush-yards  Nebraska rushing yards
opp-pass-comp  Opponent passing completions
opp-pass-att  Opponent passing attempts
opp-pass-yards  Opponent passing yards
ne-pass-comp  Nebraska passing completions
ne-pass-att  Nebraska passing attempts
ne-pass-yards  Nebraska passing yards
opp-int  Opponent interceptions thrown
opp-fum  Opponent fumbles lost
ne-int  Nebraska interceptions thrown
ne-fum  Nebraska fumbles lost
ne-pen-num  Nebraska number of penalties
ne-pen-yards  Nebraska penalty yards
spread  Point spread. A negative value means Nebraska was favored.
temp  Temperature at kickoff (Fahrenheit)
humidity  Humidity at kickoff (0.0 to 1.0)
wind-speed  Wind speed at kickoff (mph)
wind-bearing  Direction from which the wind was blowing at kickoff. Measured in degrees. North is 0 and values increase clockwise.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43580) of an [OpenML dataset](https://www.openml.org/d/43580). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43580/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43580/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43580/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

